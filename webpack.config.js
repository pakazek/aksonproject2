/* T=his file describes how to bundle files ...  */
entry: {
        app: 'src/app.ts',
        vendor: 'src/vendor.ts'
    },
    output: {
        filename: '[name].js' /* it creates as many files as entry definistion: (2 files: app.js and vendor.js) */
    }

/* 
Teach it to transform non-JavaScript file into their JavaScript equivalents with loaders. 
loaders for TypeScript and CSS as follows.
*/
rules: [{
        test: /\.ts$/, 
        loader: 'awesome-typescript-loader'
    },
    {
        test: /\.css$/,
        loaders: 'style-loader!css-loader'
    }
]